# Generated by Django 2.0.5 on 2018-09-18 09:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Easeapp', '0002_auto_20180918_0653'),
    ]

    operations = [
        migrations.AddField(
            model_name='menuitems',
            name='item_price',
            field=models.IntegerField(null=True),
        ),
    ]
