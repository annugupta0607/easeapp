# Generated by Django 2.0.5 on 2018-09-17 16:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='MenuItems',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('item_name', models.CharField(max_length=30)),
                ('item_price', models.IntegerField(max_length=10)),
            ],
        ),
        migrations.CreateModel(
            name='Outlet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('outlet_name', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='QRSite',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('qr_code', models.IntegerField(max_length=10, unique=True)),
                ('table_no', models.IntegerField(max_length=10, unique=True)),
                ('outlet', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='Easeapp.Outlet')),
            ],
        ),
        migrations.AddField(
            model_name='menuitems',
            name='outlet',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='Easeapp.Outlet'),
        ),
    ]
