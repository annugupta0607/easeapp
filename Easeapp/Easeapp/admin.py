from django.contrib import admin

from Easeapp.models import Outlet, QRSite, MenuItems

admin.site.register(Outlet)
admin.site.register(QRSite)
admin.site.register(MenuItems)