import json
from datetime import datetime

from pymongo import MongoClient


def simple_middleware(get_response):
    # One-time configuration and initialization.
    cli = MongoClient()
    db_name = "ease"
    collection = "req_logs"

    def middleware(request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        req_time = datetime.now()

        response = get_response(request)

        # Code to be executed for each request/response after
        # the view is called.
        req = request.build_absolute_uri()

        res = response.content.decode("utf-8")
        d = json.loads(res)

        res_time = datetime.now()
        execution_time = res_time - req_time

        print("Execution_time",execution_time)
        doc = {"request": req, "response": d, "requestTime": req_time, "executeSecs": execution_time.total_seconds()}

        cli[db_name][collection].insert(doc)

        return response

    return middleware
