from django.http import JsonResponse
from Easeapp.models import QRSite, MenuItems
import qrcode

#def qrimage(request):

   #qr_code = request.GET['qr_code']
   #qr = qrcode.QRCode(
    #    version=1,
     #   error_correction=qrcode.constants.ERROR_CORRECT_H,
      #  box_size=10,
       # border=4,
    #)

   #data = "127.0.0.1/Easeapp/qr_code = ", qr_code
   #qr.add_data(data)

   #qr.make(fit=True)

   #img = qr.make_image()

   #img.save("image.jpg")

   #return JsonResponse({"qr_code": "Qr Image has been created"})


def easeapp(request):
    qr_code = request.GET['qr_code']
    print ("HI")
    print(qr_code)
    try:
        one_entry = QRSite.objects.get(qr_code=qr_code)
        menu_items = MenuItems.objects.filter(outlet=one_entry.outlet)
        item_list = []
        for item in menu_items:
            item_dict = {
                'name': item.item_name,
                'price': item.item_price
            }
            item_list.append(item_dict)
        return JsonResponse({"table": one_entry.table_no, "outlet": one_entry.outlet.outlet_name,
                             "menuItem": item_list})
    except QRSite.DoesNotExist:
        return JsonResponse({"msg": "Does not exist"})

