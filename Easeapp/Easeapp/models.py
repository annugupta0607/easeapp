from django.db import models
import qrcode
from django.conf import settings
from django.conf import os
from Easeapp.settings import STATIC_ROOT


class Outlet(models.Model):
    outlet_name = models.CharField(max_length=20)


class QRSite(models.Model):
    qr_code = models.IntegerField( unique=True)
    table_no = models.IntegerField(unique=True)
    outlet = models.ForeignKey(Outlet, on_delete=models.DO_NOTHING)

    def save(self, *args, **kwargs):
        #self.qr_code
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_H,
            box_size=10,
            border=4,
        )
        data = "127.0.0.1/Easeapp/qr_code = ", self.qr_code
        qr.add_data(data)
        qr.make(fit=True)
        img = qr.make_image()
        qr_image_dir = os.path.join(STATIC_ROOT, 'qr_images')
        if not os.path.isdir(qr_image_dir):
            os.makedirs(qr_image_dir)
        with open(os.path.join(qr_image_dir, str(self.qr_code)), 'wb') as f:
            img.save(f, format="png")
        super().save(*args, **kwargs)


class MenuItems(models.Model):
    outlet = models.ForeignKey(Outlet, on_delete=models.DO_NOTHING)
    item_name = models.CharField(max_length=30)
    item_price = models.IntegerField(null=True)



